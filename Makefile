CFLAGS = -std=gnu99 -Wall -Wextra -g -O0

LDFLAGS = -lagios -lpthread -lxml2

#Plafrim includes for libraries
LDFLAGS += -L$(HOME)/.lib/ -I$(HOME)/.lib/include -I/usr/include/libxml2

AGIOSCONFPATH = AGIOS_CONF=~/agios.conf

COMPILER = mpicc
BUILDDIR = build
INSTALLDIR = install
SOURCEDIR = src
TESTDIR = tst

all: server 

%.o: $(SOURCEDIR)/%.c
	$(COMPILER) -c $(CFLAGS) -o $(BUILDDIR)/$@ $< $(LDFLAGS) 

%.o: $(TESTDIR)/%.c
	$(COMPILER) -c $(CFLAGS) -g3 -o $(BUILDDIR)/$@ $< $(LDFLAGS) 

run_server_valgrind : server
	$(AGIOSCONFPATH) mpirun -np 8 valgrind --leak-check=full --show-leak-kinds=all build/server -t 1 -s 0 -x config/valgrind.xml

run_server : server
	$(AGIOSCONFPATH) mpirun -np 8 build/server -t 1 -s 2 -x config/standard_fast.xml -d 1


server: server.o client.o timing.o queue.o request_buffer.o request.o helpers.o scheduler.o app.o bandwidth.o set_usage.o
	$(COMPILER) $(CFLAGS) $(addprefix $(BUILDDIR)/, $^) -o $(BUILDDIR)/$@ $(LDFLAGS) 

test_buffer: test_buffer.o request_buffer.o
	$(COMPILER) $(CFLAGS) -g $(addprefix $(BUILDDIR)/, $^) -o $(BUILDDIR)/$@ $(LDFLAGS) 

test_queue: test_queue.o queue.o
	$(COMPILER) $(CFLAGS) -g $(addprefix $(BUILDDIR)/, $^) -o $(BUILDDIR)/$@ $(LDFLAGS) 


clean:
	@rm -f *~ src/*~ build/*

.PHONY: clean
