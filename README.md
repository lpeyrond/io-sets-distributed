This repository contains the code for a decentralized implementation of the *IO-Sets* I/O management method, also providing the flexibility to set up various I/O scenarios in an Application-Server environment using MPI. Compilation and usage are described in file, followed by the technical report of its realization and finally its comparaison to the Set-10 implementation.    

This work originates from a 2-month summer internship in 2023 by Louis Peyrondet at Inria Bordeaux, within the TADaaM team, under the supervision of Francieli Boito and Luan Teylo.

-   The IO-Sets paper : https://hal.science/hal-03648225/

# Setup

Tested with gcc 12.2.0 and openmpi 2.0.4

- Compile the AGIOS library (https://github.com/francielizanon/agios)
- Inside this repo : 
  - Edit the agios.conf to give the path to the wfq.conf file containing the sets priorities
        
        mkdir ~/logs && mkdir build
        make server
  - If you don't want to deal with custom paths, copy the agios.conf and the wfq.conf in your ~/


# Usage

Command line exemple:

    AGIOS_CONF=~/agios.conf mpirun -np 8 build/server -t 1 -s 2 -x config/standard_fast.xml -d 1 -l "First_test"

You must have a number of MPI process -2 that is divisible by the number of application of your scenario. So for the exemple above with have 8 processes, minus 2 (server and scheduler process) equals 6, and the scenario used has 6 applications. This means that we will have 1 client per application. Recommanded amount of process/client per application is 5. A good rule of thumbs is to take the amount of application of your scenario, multiply it by 5 then add 2 to get the required number of MPI processes. Most of the tests were done with 8 threads for the consummers.

| Parameter             | Description  |
| ----------------- | ------------ |
| -t   | Values : [0,N]. Number of threads used in the consummer thread pool. This represents the number of parallel writes being done on the server disk |
| -s | Values : [0,1,2,3,4]. Synchronization flags used when creating the files. This is useful in the cases where the server node has a lot of cached memory making the first I/O request fast but inconsistant in speed which messes up the scheduling. 0=None, 1=O_DIRECT, 2=O_SYNC, 3=O_DIRECT+O_SYNC, 4=FSYNC |
| -x | Values : [string]. Path to the XML scenario to use  |
| -d | Values : [0,1]. Should it use the decentralized version (1) or not (0). The centralized version is an equivalent of the standard Set-10 scheduling|
| -l | Values : [string]. Adds this string to the generated parameters CSV. This is useful do quickly sort the generated data |
| -s | Values : [0,N]. The seed used to initialize the random number generator  |

# Scenarios

To be able to quickly create and use specific scenarios, we use a XML config file to describe each applications in a scenario. It follow the same structure as the one that were used during the IO-Sets experimentations expected the bandwidth Percentage field that is new (so that's why you'll see some simgrid keywords, but it is not linked to simgrid). 

Here's the structure of a scenario with 1 application. 

    <?xml version='1.0'?>
        <!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">
        <platform version="4.1">

        <actor host="bob" function="host">
            <argument value="app_1"/>       <!-- App Name -->
            <argument value="3" />          <!--periodicity  -->
            <argument value="60e9" />        <!-- Compute size (flop) -->
            <argument value="10000000000"/> <!-- File size (bytes) -->
            <argument value="0"/> <!-- priority  -->
            <argument value="50"/> <!-- Bandwidth Percentage-->
            <argument value="6"/> <!-- release time (s)-->
        </actor>   
    </platform>

- App name : Name of the application
- Periodicity : Number of iterations (one CPU and one IO phase)
- Compute Size : Time spent in each CPU phase, 1e9 equals to 1 second of CPU time
- File Size : Number of bytes written at each I/O phase
- Priority : Set the application belongs to
- Bandwidth Percentage : Limitation of the maximum bandwidth achievable by the application in percentage of the server max bandwidth. A value of 0 means no limitation. eg : A value of 20 with a max server bandwidth of 150MB/s means that the application sends I/O request at the rate of 30MB/s
- Release time : Time waited before starting the application

The XML is then composed of multiple applications.

# CSV Generation
Each execution outputs 4 types of CSV files.
- {JobID}_parameters.csv : Only 1 generated
- {JobID}_applications\_{appId}.csv : 1 per application
- {JobID}_phases\_{appId}.csv :  1 per application
- {JobID}_rtts\_{clientId}.csv :  1 per client

The JobId is the equivalent of a primary key in a relational database.

### Parameters CSV


| Field                      | Description                                                               |
| ------------------------- | --------------------------------------------------------------------- |
| JobId                      | SLURM job ID given to the execution|
| Param_Threads_Count        |   Number of threads used in the thread pool. This represents the number of parallel writes being done on the disk.                                                                  |
| Param_App_Count            | Amount of applications in the loaded scenario |
| Param_Clients_Count        | Sum of all the clients of the applications. |
| Param_Decentralized        | Flag telling if the program was running with the centralized (0) or decentralized (1) version. |
| Param_Sync_Mode            | Synchronization flags used. 0=None, 1=O_DIRECT, 2=O_SYNC, 3=O_DIRECT+O_SYNC, 4=FSYNC. |
| Param_XLM_Path             | Path of the XML Scenario loaded|
| Param_Benchmarked_Bandwith | Bandwidth in bytes per seconds measured during the initialization benchmark |
| Param_Bandwith_Multiplier  | Multiplier applied to the Benchmarked Bandwidth to determine the max bandwidth used by the program for the metrics|
| Param_Bandwith_Timestep    | The time, in microseconds, between each update of the server set's bandwidths|
| Param_Seed                 | Seed used for the random number generator |
| Param_Chunksize            | The size, in bytes, of the I/O requests payload |
| Param_Set_Count            | Number of sets used |
| Param_Set_Priorities       | Priorities of each set |
| Param_Label                | Label given by the user for the execution |


### Applications

In this CSV, each row represents the applications loaded. It gives the attributes of each one of them. 

| Field              | Description                                                                 |
| ----------------- | --------------------------------------------------------------------------- |
| JobId              | SLURM job ID given to the execution |
| App_Id             | Id of the application |
| App_Data           | Amount of data, in bytes, written at each I/O phase |
| App_Cpu_Time       | Time, in seconds, spent computing (sleeping) at each CPU phase |
| App_Bandwidth_Limit| Limitation of the maximum bandwidth achievable by the application in percentage of the server max bandwidth. A value of 0 means no limitation. eg : A value of 20 with a max server bandwidth of 150MB/s means that the application sends I/O request at the rate of 30MB/s   |
| App_Set            | Set used by the application for I/O |
| App_Periodicity    | Number of phases. eg : A value of 2 means that the application will do 2 CPU phases and 2 I/O phases |
| App_Release_Time   | Time, in seconds, before the application starts |
| App_Name           | Name given to the app in the scenario XML |

### Phases

In this CSV, each row represents a phase for the application.

| Field                     | Description  |
| ------------------------- | ------------ |
| JobId                     | SLURM job ID given to the execution |
| Phase_Time                | Time, in microseconds, spent in this phase |
| Phase_Start               | Time, in microseconds, at which the phase started  |
| Phase_Id                  | Phase number |
| Phase_Type                | Type of the phase, 0=CPU, 1=I/O |
| Phase_App_Id              | Id of the application doing the phase |
| Phase_Master_Client_Rank | Id of the master client of the application  |


### Rtts
In this CSV, each row represents a I/O request made by the client. 

| Field             | Description  |
| ----------------- | ------------ |
| Rtt_Client_rank   | Rank of the client |
| Rtt_Time          | Time, in microseconds, of the round trip time (communication to server + scheduling and writing on the server disk + communication back to the client) |
| Rtt_Time_Sent     | Time, in microseconds, at which the request was sent by the client  |
| Rtt_Request_Id    | Id of the request. The first request has Id 0, next one is Id 1 ... |
| Rtt_Phase_Id      | Which I/O phase was the request in |
| Rtt_App_Id        | Id of the application making the request  |
| Rtt_Set_Id        | Set used by the request |



# *REPORT*

## The Program 
To quickly explain how the program work to write the data : It divide the total amount of data **filesize** in equal chunk for each clients. The server creates a file for each client and then wait for the request to write inside them. Each clients send requests that consist of metadata about the operation (len, offset ...) and the payload, configured to 512Kb, containing the data. When the server receive a request from a client, it give the request to AGIOS and then sends a acknowledgment back to the client. Once the last request of a client has been processed it stores the current time for the client ending. When every request has been processed it then generates a CSV log file.

![Diagram of the program](img/diagram_program.png)


## First Tests and Bandwidth Peak
I first launched the tests without the 64gb sizes as it looked like my program was misbehaving with them. Here are the results :

![Parameters Combinaison up to 32GB](img/bandwidth_parameters_buffered.png)

We can see that the differents combinaison of parameters give close results, but that in average 4 clients seems to always be better. The highest bandwith was obtained by using 4 clients with 64 threads.

Seeing that lower amounts of clients seems to yield better bandwith I also tested it with 1 and 2 clients to see the result : 

![Low Client Counts](img/bandwidth_parameters_low_clients.png)

The graph starts to get a little messy but we can observe that a low amount of client doesn't equal better bandwidth and that 4 clients seems to be a good middle-ground.

But still we're getting an insane bandwidth (~4GB/s) for a hard drive, so maybe my program wasn't misbehaving so I went and got the results for 64gb file size :

![Bandwidth 64gb](img/bandwidth_parameters_64.png)

Every combinaison drops to ~240MB/s which is way closer to the real speed of a HDD. So it suggests that for the filesize <= 32Gb they are "cached" and doesn't represent the actual bandwidth of the disk as it's not synchronized. So I modified the program to add the option of using using the *fsync()* function that waits until the given file is actually writted into the disk. Knowning that 64 thread and 4 clients seemed like the best combinaison I tried them with the fsync option enabled.


![Bandwidth Sync](img/bandwidth_parameters_fsync.png)

Now we're getting the actual bandwidth of the disk which is around 120MB/s. Yet the results without sync are nearly 2 times betters at 240MB/s. That is due once again to the buffer as it is not removed, it still has some effect on the 64GB file. So I increased the **filesize** to 128 and 256 GB to see if the bandwidth would plateau toward the 120MB/s limit as the effect of the buffer would start to be negligeable.


![Bandwidth Sync Plateau](img/bandwidth_parameters_fsync_256.png)

As expected the effects of the buffer/cache are starting to completely disappear with the bigger filesizes. For 256GB, the sync version is at ~120MB/s and the non-sync version is at ~130MB/s.


Maybe the payload of the request (512Kb) was too small to max out the disk speed as this mean that writes calls are only writing up to 512Kb at a time. So I increased it to 1Mb to see if there was an impact.


![Higher Payload](img/bandwidth_parameters_payload_size.png)

As we can see increasing the request/writing size didn't have an effect on the writing speed. 

## Getting the sharing to work

The problem with the previous implementation is that it used a buffer to store the incoming requests until it was full. This method allows the server to have multiple requests from multiple applications at the same time allowing AGIOS to do some scheduling. This implementation have 2 problems:
- When the buffer is full it has to wait until some space is available. In our case the server was waiting until half of the buffer was emptied before taking new request. This behavior results in the sharing not working correctly as all the requests of higher priorities were emptied before the others, resulting in a buffer emptied of high priority requests and having only lower priorities ones, yet not taking new incoming high priority requests.
- This is not how a PFS works.

The correct way to implent it is to have the client wait for an acknowledgment from the server before sending another request. The server should send the acknowledgment back to client only once the client request has been processed (written) on the disk. This way the buffer only holds 1 request per client at the same time, so at most nb_of_clients requests in the buffer.

To measure the sharing, we use a new metric Round Trip-Time (RTT) for each request that represent the time is takes for the request to be processed on the server. It's the difference between the timestamp taken when the request is sent by the client, and the timestamp when the acknowledgment is received by the client. This way low priority application should have a higher RTT than the high priority ones.

The scenario used to observe bandwidth sharing is the following : We have 3 applications, 3 sets, 1 applications per set. The set priorities are 800, 3300, 5800 meaning (roughly) that set 0 receives 8% of the bandwidth, set 1 33% and set 2 58%. The 1st applications in set 0 writes 10Gb, the 2nd application in set 1 write 41.25Gb and for set 3 the 3rd application write 72.5Gb. If the sharing is working as intended then the 3 applicatons should finish their I/O phase at the same time, otherwise app 0 will end first, followed by app 1 then app2.

Here is the sharing using the new implementation. Each dot is a request sent a client. The Y axis is the RTT of the request (higher means slower bandwidth), and the X axis is the time the request was sent. Each request colored by its set id.

![Sharing Big Queue](img/rtt_8thread_30clients_10gig_0sync_3app_bigQueue.png)

From the graph we can see that the applications do not end at the same time which means that the sharing is not working. The only sharing applied here looks to be a fair-sharing , where each application get a equal amount of the bandwith. We can clearly observe that when the application in set 1 finishes (at ~500sec), the only remaining application is the one from set 2 and the RTTs of its requests drop down as it doesn't have to share the bandwith with others applications anymore. 
The beginning of the graph shows really fast RTTs as it uses the node cache for the I/O requests averaging 4GB/s but with a lot of variations, once it's full we can start seeing the real RTTs. To remove this effect we can add synchronization such as the O_SYNC flag which lets us really see this fair-sharing behavior.

![Sharing Big Queue Sync](img/rtt_8thread_30clients_10gig_2sync_3app_bigQueue.png)


We can see that the 3 applications are sharing bandwidth from 0 to ~800 seconds, then the application in set 0 ends which gives more bandwidth to applications 1 and 2. At ~2600 seconds application 1 ends which gives full bandwith to application 2. 

So why isn't the sharing working ? This comes down to the implementation : Once AGIOS gives back a request, its inserted into a queue from where the thread pool will get the request to write them on the disk. The problem is that this queue **needs** to be of size 1, so that AGIOS can stock up on requests and schedule them correctly. If the queue is bigger than the number of clients, this means that AGIOS will not do any work as the requests are all immediatly stored into the queue once it receives them. What happends afterward is that the first request of the queue is processed, the acknoledgment is sent back to the client that sends a new request that will just ends up at the end of the queue. This explain the behavior of fair-sharing that we observed.

So what does the sharing looks like with this fix ?

![Sharing fix](img/rtt_8thread_30clients_10gig_0sync_3app.png)


The sharing seems to be working but we should have the 3 applications that end at the same time, yet this isn't the case here. This is once again due to the node cache that make the first requests have very low but highly variable RTTs. We can use the synchronization to really make sure that the sharing is working as intended and that the difference of ending time is only due to the node cache.

![Sharing fix Sync](img/rtt_8thread_30clients_10gig_2sync_3app.png)

## Exclusive

Now that the sharing part is working correctly, we can work on the exclusive part : if 2 applications in the same set want to do I/O at the same time then only one of them will be authorized and the others will have to wait until it has finished.
As we want a baseline to compare, we are for now using a centralized scheduler that will handle this exclusive distribution. The goal of the internship is to get rid of these centralized parts.

To implement this, one of the processes is designated as the scheduler. Each application, composed of multiple clients, will have one of its client tagged *master* that will handle the communication with the scheduler. Then before each I/O phase the master client asks the scheduler the permission to do I/O, and the scheduler responds once it is allowed to do so.

To observe the exclusive part, we use the same scenario as the sharing one, with the only difference that we now have 2 applications per set. In the generated graph, each color is a set and the different shade of the color represent the differents applications in the set.
![Sharing and Exclusive ](img/rtt_8thread_120clients_10gig_0sync_6app.png)

This graph shows us that both the sharing and exclusive part are working as intended. When a application end, the other application starts just after so we never have 2 applications from the same set doing I/O at the same time.  Another interesting part of the graph is that we can see that even when the bandwidth of the disk has some variations, the varations is also applied to the sets proportionnal to their priorities. Finally at ~1700 seconds we can see a huge spike in RTT over all the applications meaning that during this small period the bandwidth of the disk was greatly reduced. A plausible explaination is that the disk had a fragmention operation do to at this time.


## Making a Decentralized version
Now that we can have a certain level of assurance of the program functionality, we can start thinking about how should the decentralized version work.

The idea is that when an application sends an I/O request, the acknowledgement packet from the server contains a flag that tells the application if it can continue I/O requests or if it should stop and backoff. To do that we need some kind of metric on the server. 

The metric that was finally used is to compute the bandwidth of each set and to compare it to the max bandwidths given the active sets. Given the set that are currently being used and their priorities we can pre-compute the total bandwidth repartion between them. For exemple with 3 sets with priorities [800, 3300, 5800] we get this table, with rows showing the sets being used (meaning that 000 (Big Endian) is no set being used, 001 only set 2 is used and 111 all set are used) and the columns the percentage of the bandwidth that the set should have : 

|  | % Bandwidth set 0  | % Bandwidth set 1 |  % Bandwidth set 2 |
| :----- |:-----:|:-----:|:-----:|
|  000 | 100 |  100 |  100 |
|  001 | 0  |  0  |  100  |
|  010 | 0  |  100  |  0  |
|  011 | 0  |  ~36  |  ~64  |
|  100 | 100  |  0  |  0  |
|  101 | ~12 |  0  |  ~87  |
|  110 | ~19.2  |  80  |  0  |
|  111 | 8 |  33  |  58  |

The bandwidth of each set is computed by a background thread where every 'timestep' microseconds elapsed, it gets the total amount of data written in each set and multiplies it by (1sec/timestep) to get the bandwidth in bytes per seconds of each set. It then resets the counter of data in each set back to 0.

Then when the server sends back the acknowledgement to the clients, it gets the percentage allowed to the set, applies it to the benchmarked bandwidth then compares it to the set current bandwidth. If the current set bandwidth is greater then it denies the I/O phase of the client, else it allows the I/O phase of the client to begin.

Here is a graph showing the exclusive access of the set when 2 applications that uses 100% of the bandwidth tries to do I/O at the same time. We can observe the I/O request of the second application asking for permission and getting denied, increasing the backoff between each request until is gets accepted.

![Exclusive Decentralized](img/decentralized_exclusive.png)


This still is a exploratory approach as it has some flaws. First of all if set 0 and set 1 are supposed to share the bandwidth at 50/50 but set 1 bandwidth cannot reach 50 then even if set 0 is getting more requests that could use this bandwidth is would get denied. The other flaw is that computing the bandwidth over a time window can lead to some problems : the most obvious one is that if every application start at the same time, all the I/O request asking for permission will come in at the same time and every one of them will get accepted as the window didn't finish so the current bandwidth never got updated.

Still we know that in real world uses, application rarely hit the peak of the bandwidth available meaning that the benefits of this decentralized version is that multiple application can share the bandwidth of the set. This was not the case of the centralized one and this graph highligths the difference in total execution time it makes between the 2 versions with applications that have a limited bandwidth.


![Comparaison Decentralized & Centralized](img/comparaison_decentralized_centralized.png)

With bandwidth Limit in % of the server maximum bandwidth, 0 being no restriction, 20 being limited to 20% of the max bandwidth. 

