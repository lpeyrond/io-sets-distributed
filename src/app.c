#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string.h>
#include <assert.h>

#include "app.h"
#include "helpers.h"


void processXMLElementCounter(xmlNodePtr node, int *app_count) {
    xmlNodePtr cur;

    for (cur = node; cur != NULL; cur = cur->next) {
        if (cur->type == XML_ELEMENT_NODE) {
            if (strcmp((const char *)cur->name, "actor") == 0){
                *app_count += 1;
            }else{
                processXMLElementCounter(cur->children, app_count);
            }
        }
    }
}

int app_count_xml(const char * filename){
    int app_count = 0;
    xmlDocPtr doc;
    xmlNodePtr root;

    // Open the XML file
    doc = xmlParseFile(filename);

    if (doc == NULL) exit_error("Couldn't open the XML file\n", __FILE__, __LINE__);

    // Get the root element
    root = xmlDocGetRootElement(doc);

    // Process the XML tree
    processXMLElementCounter(root, &app_count);

    // Cleanup
    xmlFreeDoc(doc);

    return app_count;
}

void convertXMLActorToApplication(xmlNodePtr node, app_t *app){
    int arg_count = 0;
    app->bandwidth_percentage = 0.0;
    node = node->children;
    while(node != NULL){
        if (node->type == XML_ELEMENT_NODE) {
            xmlChar*value = xmlNodeGetContent(node->properties->children);
            // printf("%s\n", (const char*)value);
            
            switch (arg_count)
            {
            case 0:
                size_t min_len = sizeof(app->name) > strlen((const char*)value) ? strlen((const char*)value) : sizeof(app->name);
                strncpy(app->name, (const char*)value, min_len);
                break;
            case 1:
                app->periods = atoi((const char*)value);
                break;
            case 2:
                double cpu_flops = atof((const char*)value);
                app->cpu_time = (int) (cpu_flops/1000/1000/1000);
                break;
            case 3:
                if (sscanf((const char*)value, "%zu", &app->total_bytes) != 1) exit_error("Couldn't read XML file size\n", __FILE__, __LINE__);
                break;
            case 4:
                app->set = atoi((const char*)value);
                break;
            case 5:
                app->bandwidth_percentage = atof((const char*)value);
                break;
            case 6:
                app->release_time = atoi((const char*)value);
                break;
            
            default:
                break;
            }
            arg_count++;
            xmlFree(value);
        }
        node = node->next;
    }
}

void processXMLApplications(xmlNodePtr node, app_t *apps) {
    xmlNodePtr cur;
    int cur_app = 0;
    for (cur = node; cur != NULL; cur = cur->next) {
        if (cur->type == XML_ELEMENT_NODE) {
            if (strcmp((const char *)cur->name, "actor") == 0){
                convertXMLActorToApplication(cur, apps + cur_app);
                apps[cur_app].id = cur_app;
                cur_app++;
            }else{
                processXMLApplications(cur->children, apps);
            }
        }
    }
}


app_t *app_read_xml(const char * filename, int app_count){
    app_t * apps = malloc(sizeof(*apps) * app_count);
    assert(apps);


    xmlDocPtr doc;
    xmlNodePtr root;

    // Open the XML file
    doc = xmlParseFile(filename);

    if (doc == NULL) exit_error("Couldn't open the XML file\n", __FILE__, __LINE__);

    // Get the root element
    root = xmlDocGetRootElement(doc);

    // Process the XML tree
    processXMLApplications(root, apps);

    // Cleanup
    xmlFreeDoc(doc);

    return apps;
}


void app_free(app_t *apps){
    if (apps) free(apps);
}


void app_print(app_t app){
    printf("name : %s, id : %d, set : %d, periods : %d, data : %zu, cpu time : %d, release time %d, banwidth_percentage : %.2lf %%\n", app.name, app.id, app.set, app.periods, app.total_bytes, app.cpu_time, app.release_time, app.bandwidth_percentage);
}
