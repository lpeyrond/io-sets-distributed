#pragma once

#include <stdlib.h>

void set_usage_init(size_t set_count);

size_t set_usage_get_state();

void set_usage_inc_set(int set_id);

void set_usage_dec_set(int set_id);

void set_usage_free();

double** set_usage_compute_bandwidth_table(int num_sets, int* priorities);

void set_usage_free_bandwidth_table(double** bandwidth_table, int num_sets);
double set_usage_get_bandwidth_percentage(double** bandwidth_table, size_t selected_sets, int set_id);
