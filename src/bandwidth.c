#define _GNU_SOURCE
#include <pthread.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "bandwidth.h"
#include "server.h"
#include "config.h"
#include "helpers.h"

size_t *bandwidth_set;
size_t *bandwidth_data_set;
size_t _set_nums;
pthread_mutex_t *bandwidth_data_mutexes;
pthread_mutex_t *bandwidth_mutexes;

void bandwidth_init(size_t set_count){
    _set_nums = set_count;
    bandwidth_set = calloc(set_count, sizeof(*bandwidth_set));
    assert(bandwidth_set);

    bandwidth_data_set = calloc(set_count, sizeof(*bandwidth_data_set));
    assert(bandwidth_data_set);

    bandwidth_data_mutexes = calloc(set_count, sizeof(*bandwidth_data_mutexes));
    assert(bandwidth_data_mutexes);
    for (size_t i = 0; i < set_count; i++)
        pthread_mutex_init(&bandwidth_data_mutexes[i], NULL);

    bandwidth_mutexes = calloc(set_count, sizeof(*bandwidth_mutexes));
    assert(bandwidth_mutexes);
    for (size_t i = 0; i < set_count; i++)
        pthread_mutex_init(&bandwidth_mutexes[i], NULL);
}


void bandwidth_data_inc(int set_id, size_t data_size){
    pthread_mutex_lock(bandwidth_data_mutexes+set_id);
    bandwidth_data_set[set_id] += data_size;
    pthread_mutex_unlock(bandwidth_data_mutexes+set_id);
}

void bandwidth_update(size_t timestep){
    for (size_t i = 0; i < _set_nums; i++){
        pthread_mutex_lock(bandwidth_data_mutexes+i);
        pthread_mutex_lock(bandwidth_mutexes+i);
        // printf("data in set %zu : %zu\n", i, bandwidth_data_set[i]);
        bandwidth_set[i] = bandwidth_data_set[i] * (1000000.0/timestep);
        bandwidth_data_set[i] = 0;
        // printf("new bd for set %zu : %zu\n", i, bandwidth_set[i]);
        pthread_mutex_unlock(bandwidth_data_mutexes+i);
        pthread_mutex_unlock(bandwidth_mutexes+i);
    }
}

size_t bandwidth_current(int set_id){
    pthread_mutex_lock(bandwidth_mutexes+set_id);
    size_t bd = bandwidth_set[set_id];
    pthread_mutex_unlock(bandwidth_mutexes+set_id);
    return bd;
}


void bandwidth_free(){
    if (bandwidth_set) free(bandwidth_set);
    if (bandwidth_data_set) free(bandwidth_data_set);
    if (bandwidth_mutexes) free(bandwidth_mutexes);
    if (bandwidth_data_mutexes) free(bandwidth_data_mutexes);
}


//Returns an approximation of the disk bandwidth in bytes per seconds
size_t benchmark_max_bandwidth(int synchronize){
    size_t benchmark_size = BANDWIDTH_BENCHMARK_GIGABYTES * 1000000000ULL; 
    char *buffer = calloc(sizeof(*buffer), benchmark_size);
    char filename[100];
    sprintf(filename, "%s/file_benchmark", OUTPUT_PATH);

    int flags = O_WRONLY | O_CREAT | O_TRUNC;
    if (synchronize == SYNC_ODIRECT) flags = flags | O_DIRECT;
    if (synchronize == SYNC_OSYNC) flags = flags | O_SYNC;
    if (synchronize == SYNC_ODIRECT_OSYNC) flags = flags | O_DIRECT | O_SYNC;

    int bench_file = open(filename, flags, 0644);
    if (bench_file == -1) exit_error("Couldn't open benchmark file\n", __FILE__, __LINE__);

    double start = timer_get_time_seconds();
    ssize_t bytes_written = 0;
    if(synchronize == SYNC_ODIRECT){
        size_t alignment = sysconf(_SC_PAGESIZE); // Use the system's page size
        char *buffer_aligned = NULL;
        if (posix_memalign((void **)&buffer_aligned, alignment, benchmark_size) != 0) {
            perror("aligned buffer allocation failed");
            exit(EXIT_FAILURE);
        }
        assert(buffer_aligned != NULL);
        memcpy(buffer_aligned, buffer, benchmark_size);
        bytes_written = write(bench_file, buffer_aligned, benchmark_size);
        free(buffer_aligned);
    }else{
        write(bench_file, buffer, benchmark_size);
    }

    if(synchronize == SYNC_FSYNC){
        fsync(bench_file);
    } 

    double end = timer_get_time_seconds();
    free(buffer);
    close(bench_file);
    if (bytes_written == -1) exit_error("Couldn't write the benchmark file\n", __FILE__, __LINE__);
    double difference = end - start;

    return benchmark_size/difference;
}
