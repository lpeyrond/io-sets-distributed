//Path were the dummy file will be written
#define OUTPUT_PATH "/tmp"

//Size of the queue where requests are stored once AGIOS scheduled them. This is the queue from where the consumers thread will get the requests
#define REQUEST_QUEUE_SIZE 1

//Size of the internal buffer used to stored the request to be able to retrived them once they have been scheduled
#define REQUEST_BUFFER_SIZE 1500

//Number of sets used. The wfq.conf file should have at least as many priorities in it than this number
#define NUMBER_OF_SETS 3

//Priorities assigned to each set. Should be the same values as the wfq.conf file used
#define SETS_PRIORITIES {800, 3300, 5800}

//Size of the payload of the requests. Effective size of the payload is CHUNKSIZE*KILOBYTES
#define CHUNKSIZE 512
#define KILOBYTES 1000

//Number of gigabytes written for the bandwidth benchmark
#define BANDWIDTH_BENCHMARK_GIGABYTES 2

//Time, in microseconds, elapsed between each new computation of the current sets bandwidths  
#define BANDWIDTH_COMPUTATION_TIMESTEP_MICROSECONDS 1000000

//Multiplier used on the benchmarked bandwidth value to get the actually used max bandwidth. This is done as we expected benchmarked value to be higher and to have a marge of error 
#define BANDWIDTH_MULTIPLIER 0.90