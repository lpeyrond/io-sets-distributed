#pragma once

#include <stdlib.h>

/// @brief Creates one client process. Keep in mind that an application is composed of multiple clients therefore, for exemple, the file_size is not the amount of data described in the scenario but rather the amount of data described in the scenario divided by the number of clients per application
/// @param file_size Size, in bytes, of the I/O phase for the client
/// @param file_offset Offset at which the client will starting writing the data
/// @param num_phases Number of phases
/// @param set_id Set used by the client
/// @param rank Rank of the client process
/// @param app_id Id of the application the client belongs to
/// @param cpu_time_sec Time, in seconds, spent sleeping to simulate the CPU phase
/// @param scheduler_rank Rank of the scheduler process
/// @param master_rank Rank of the master client for the application
/// @param app_comm Allows communication between clients of the same application
/// @param release_time_sec Time, in seconds, before the client starts 
/// @param bandwidth_cap Limit, in bytes per seconds, the amount of data send by the client 
/// @param decentralized If the program is running in decentralized mode or not
void client(size_t file_size, size_t file_offset, int num_phases, int set_id, int rank, int app_id, int cpu_time_sec, int scheduler_rank, int master_rank, MPI_Comm app_comm, int release_time_sec, size_t bandwidth_cap, bool decentralized);