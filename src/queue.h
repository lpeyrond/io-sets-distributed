#pragma once
#include <stdbool.h>
#include <pthread.h>

#include "request.h"

typedef struct queue {
    size_t writeIndex;
    size_t readIndex;
    size_t max_length;
    size_t element_count;
    void **elements;
    pthread_mutex_t mutex;
} queue_t;

queue_t *queue_create(size_t length);
void queue_enqueue(queue_t* q, void *element);
void * queue_dequeue(queue_t* q);
bool queue_is_empty(queue_t* q);
bool queue_is_full(queue_t* q);
void queue_free(queue_t* q);
void queue_print(queue_t* q);

