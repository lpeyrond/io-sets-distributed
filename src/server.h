#pragma once

#include <stdint.h>
#include <limits.h>

#if SIZE_MAX == UCHAR_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
   #define my_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#else
   #error "SIZE_MAX is undefined"
#endif

//Reponse code from the server to the clients
#define SUCCESS 1
#define WAIT 2

//Maximum amount of sets as it is working with bitwise operations on size_t variables (Depending on the system/machine this could go wrong !!) 
#define MAXIMUM_NUMBER_OF_SETS 63

enum sync_types {
    SYNC_NONE = 0,
    SYNC_ODIRECT = 1,
    SYNC_OSYNC = 2,
    SYNC_ODIRECT_OSYNC = 3,
    SYNC_FSYNC = 4
};


extern int slurm_job_id;

