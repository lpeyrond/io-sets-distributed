#pragma once

#include <stdlib.h>
#include <time.h>

#define PREFERRED_CLOCK CLOCK_MONOTONIC

extern struct timespec timer_program_start;

size_t timespec_to_microseconds(struct timespec ts);
void timer_timestamp_start(struct timespec *ts);
struct timespec * timer_init(int num_clients);
double timer_get_time_seconds(void);
size_t timer_get_time_microseconds(void);
void timer_timestamp(struct timespec * times, size_t index);
void timer_free(struct timespec * times);
