#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include "scheduler.h"
#include "queue.h"
#include "helpers.h"

MPI_Datatype mpi_scheduling_info_t;

MPI_Datatype scheduling_info_create_mpi_type(){
    MPI_Datatype mpi_scheduler_requests_t;

    int blocklengths[] = {1, 1, 1, 1};  // Number of elements in each block
    MPI_Datatype types[] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT};
    MPI_Aint offsets[] = {
        offsetof(scheduling_info_t, flag),
        offsetof(scheduling_info_t, client_rank),
        offsetof(scheduling_info_t, app_id),
        offsetof(scheduling_info_t, set_id)
    };
    MPI_Type_create_struct(4, blocklengths, offsets, types, &mpi_scheduler_requests_t);

    return mpi_scheduler_requests_t;
}


void scheduler(size_t set_count){
    //wait on reicv, get request, check sets, respond or make wait
    
    bool set_available[set_count];
    
    queue_t *waiting[set_count];
    for (size_t i = 0; i < set_count; i++){
        set_available[i] = true;
        waiting[i] = queue_create(150);
    }


    while(1)
    {   
        scheduling_info_t message;
        if (MPI_Recv(&message, 1, mpi_scheduling_info_t, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS) exit_error("Scheduler couldn't get message", __FILE__, __LINE__);

        if(message.flag == ASK_PERMISSION){
            if (set_available[message.set_id]){
                //send approval
                set_available[message.set_id] = false;
                LOG("Scheduler sending approval for set %d to client %d for app %d\n", message.set_id, message.client_rank, message.app_id);
                int return_code = 1;
                if (MPI_Send(&return_code, 1, MPI_INT, message.client_rank, 0, MPI_COMM_WORLD) != MPI_SUCCESS) exit_error("Scheduler couldn't send approval to client", __FILE__ , __LINE__);
            }else{
                //enqueue the rank that asked for permission into the set queue
                int *rank = malloc(sizeof(*rank));
                assert(rank);
                *rank = message.client_rank;
                queue_enqueue(waiting[message.set_id], rank);
            }

        }else if (message.flag == PHASE_FINISHED){
            LOG("Scheduler got message for phase ending for set %d with client %d for app %d\n", message.set_id, message.client_rank, message.app_id);
            //if queue empty, open set_availability
            //else send approval to app in queue
            if (queue_is_empty(waiting[message.set_id])){
                set_available[message.set_id] = true;
            }else{
                int *rank = queue_dequeue(waiting[message.set_id]);
                int return_code = 1;
                LOG("Scheduler is sending message for permission to write for set %d with client %d for app ??\n", message.set_id, *rank);
                if (MPI_Send(&return_code, 1, MPI_INT, *rank, 0, MPI_COMM_WORLD) != MPI_SUCCESS) exit_error("Scheduler couldn't send approval to client", __FILE__ , __LINE__);
                free(rank);
            }
        }else if (message.flag == EXECUTION_FINISHED){
            LOG("Scheduler received terminating message\n");
            break;
        }else{
            assert(false);
        }

    }

    for (size_t i = 0; i < set_count; i++)
        queue_free(waiting[i]);
}