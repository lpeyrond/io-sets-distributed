#pragma once

#include <stdio.h>
#include <stdbool.h>
#include "timing.h"

#define LOG(format, ...) \
    do { \
        printf("%.6lf [%s:%d] " format "", timer_get_time_seconds(), __FILE__, __LINE__, ##__VA_ARGS__); \
    } while (0)


void exit_error(char * error_msg, const char * filename, int line);
int count_files_in_folder(const char* folder_path);
bool is_running_in_cluster();
void set_execution_id(int rank, int server_rank);