#include <assert.h>
#include <pthread.h>
#include <stdbool.h>

#include "set_usage.h"

size_t *set_queue_elements;
size_t _set_nums_usage;
pthread_mutex_t *set_queue_mutexes;

void set_usage_init(size_t set_count){
    _set_nums_usage = set_count;
    set_queue_elements = calloc(set_count, sizeof(*set_queue_elements));
    assert(set_queue_elements);

    set_queue_mutexes = calloc(set_count, sizeof(*set_queue_mutexes));
    assert(set_queue_mutexes);
    for (size_t i = 0; i < set_count; i++)
        pthread_mutex_init(&set_queue_mutexes[i], NULL);
}


void set_usage_inc_set(int set_id){
    pthread_mutex_lock(set_queue_mutexes+set_id);
    set_queue_elements[set_id]++;
    pthread_mutex_unlock(set_queue_mutexes+set_id);
}

void set_usage_dec_set(int set_id){
    pthread_mutex_lock(set_queue_mutexes+set_id);
    set_queue_elements[set_id]--;
    pthread_mutex_unlock(set_queue_mutexes+set_id);
}

size_t set_usage_get_state(){
    size_t state = 0;
    for (size_t i = 0; i < _set_nums_usage; i++){
        pthread_mutex_lock(set_queue_mutexes+i);
        if (set_queue_elements[i] != 0){
            state += 1 << i;
        }
        pthread_mutex_unlock(set_queue_mutexes+i);
    }

    return state;
}

void set_usage_free(){
    if (set_queue_elements) free(set_queue_elements);
    if (set_queue_mutexes) free(set_queue_mutexes);
}

//Computes the percentage table of each scenario
double** set_usage_compute_bandwidth_table(int num_sets, int* priorities) {
    // Calculate the total number of set configurations (2^num_sets)
    int total_configurations = 1 << num_sets;

    // Allocate memory for the 2D array
    double** bandwidth_table = (double**)malloc(total_configurations * sizeof(double*));
    for (int i = 0; i < total_configurations; i++) {
        bandwidth_table[i] = (double*)malloc(num_sets * sizeof(double));
    }

    for (int i = 0; i < total_configurations; i++) {
        size_t total_priority = 0;
        for (int j = 0; j < num_sets; j++) {
            if (i & (1 << j)) {
                total_priority += priorities[j];
            }
        }

        for (int j = 0; j < num_sets; j++) {
            if (i & (1 << j)) {
                bandwidth_table[i][j] = (double)priorities[j] / total_priority * 100.0;
            } else {
                bandwidth_table[i][j] = 0.0;
            }
        }
    }

    return bandwidth_table;
}

// Function to free the allocated memory for the 2D array
void set_usage_free_bandwidth_table(double** bandwidth_table, int num_sets) {
    for (int i = 0; i < (1 << num_sets); i++) {
        free(bandwidth_table[i]);
    }
    free(bandwidth_table);
}

double set_usage_get_bandwidth_percentage(double** bandwidth_table, size_t selected_sets, int set_id) {
    return bandwidth_table[selected_sets][set_id];
}