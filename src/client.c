#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <agios.h>

#include "request.h"
#include "server.h"
#include "client.h"
#include "timing.h"
#include "helpers.h"
#include "scheduler.h"

int send_request(char *filename, size_t len, size_t offset, size_t type, size_t set, int flag, int rank, size_t app_id){
    request_t *req = request_create(filename, len, offset, type, set, rank, flag, app_id);

    if (MPI_Send(req, 1, mpi_request_t, 0, 0, MPI_COMM_WORLD) != MPI_SUCCESS){
        fprintf(stderr, "Client %d: Couldn't send request message\n", rank);
        exit(1);
    }
    free(req);
    // printf("Client %d send resquest n%zu for file %zu (size %zukb)\n", rank, j, i, file_size/KILOBYTES);

    int return_code = 0;
    // LOG("Client %d is waiting for response for server\n", rank);
    if (MPI_Recv(&return_code, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS){
        fprintf(stderr, "Client %d: Couldn't receive a message\n", rank);
        exit(EXIT_FAILURE);
    }
    // LOG("Client %d got response %d\n", rank, return_code);
    if (!(return_code == SUCCESS || return_code == WAIT)){
        fprintf(stderr, "Client %d: Return code from server is unknown\n", rank);
        exit(EXIT_FAILURE);
    }

    return return_code;
}


void ask_permission_scheduler(int application_id, int scheduler_rank, int client_rank, int set_id){
    LOG("App %d is asking scheduler for permission on set %d\n", application_id, set_id);
    scheduling_info_t message = {.app_id = application_id, .client_rank = client_rank, .flag = ASK_PERMISSION, .set_id = set_id};
    
    if (MPI_Send(&message, 1, mpi_scheduling_info_t, scheduler_rank, 0, MPI_COMM_WORLD) != MPI_SUCCESS) exit_error("Client couldn't ask permission to scheduler", __FILE__ , __LINE__);

    //waiting for the response
    int return_code = -1;
    if (MPI_Recv(&return_code, 1, MPI_INT, scheduler_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS) exit_error("Client couldn't receive permission from scheduler", __FILE__ , __LINE__);
}

void notify_phase_end_scheduler(int application_id, int scheduler_rank, int client_rank, int set_id){
    scheduling_info_t message = {.app_id = application_id, .client_rank = client_rank, .flag = PHASE_FINISHED, .set_id = set_id};

    if (MPI_Send(&message, 1, mpi_scheduling_info_t, scheduler_rank, 0, MPI_COMM_WORLD) != MPI_SUCCESS) exit_error("Client couldn't notify end to scheduler", __FILE__ , __LINE__);

}

void write_rtts(size_t *rtts, size_t *execution_times, size_t count, int rank, int set_id, int app_id, size_t requests_per_phase){
    char filename[150];
    sprintf(filename, "%s/logs/%d_rtts_%d.csv", getenv("HOME"), slurm_job_id, rank);

    FILE *csv_file = fopen(filename, "w");
    if (csv_file == NULL) {
        fprintf(stderr, "Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fprintf(csv_file, "Rtt_Client_rank, Rtt_Time, Rtt_Time_Sent, Rtt_Request_Id, Rtt_Phase_Id, Rtt_App_Id, Rtt_Set_Id\n");  // Header row
    
    for (size_t i = 0; i < count; i++)
    {
        fprintf(csv_file, "%d, %zu, %zu, %zu, %zu, %d, %d\n", rank, rtts[i], execution_times[i], i, i/requests_per_phase, app_id, set_id);
    }
    
    fclose(csv_file);
}

void write_periods(size_t *io_periods, size_t *cpu_periods, size_t *execution_periods, size_t count, int rank, int app_id){
    char filename[150];
    sprintf(filename, "%s/logs/%d_phases_%d.csv", getenv("HOME"), slurm_job_id, app_id);

    FILE *csv_file = fopen(filename, "w");
    if (csv_file == NULL) {
        fprintf(stderr, "Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fprintf(csv_file, "JobId, Phase_Time, Phase_Start, Phase_Id, Phase_Type, Phase_App_Id, Phase_Master_Client_Rank\n");  // Header row
    
    for (size_t i = 0; i < count; i++)
    {
        if (i%2 == 0){
            fprintf(csv_file, "%d, %zu, %zu, %zu, %d, %d, %d\n", slurm_job_id, cpu_periods[i/2], execution_periods[i], i, 0, app_id, rank);
        }else{
            fprintf(csv_file, "%d, %zu, %zu, %zu, %d, %d, %d\n", slurm_job_id, io_periods[i/2], execution_periods[i], i, 1, app_id, rank);
        }
    }
    
    fclose(csv_file);
}
void client(size_t file_size, size_t file_offset, int num_phases, int set_id, int rank, int app_id, int cpu_time_sec, int scheduler_rank, int master_rank, MPI_Comm app_comm, int release_times_sec, size_t bandwidth_cap, bool decentralized){
    bool is_master = rank == master_rank;
    LOG("Client %d is up for app %d, will write %zu bytes, is application master : %s. Will release in %d seconds. Bandwidth capped at %.3lf MB/s\n", rank, app_id, file_size, is_master ? "yes" : "no", release_times_sec, bandwidth_cap/1000.0/1000.0);

    sleep(release_times_sec);
    MPI_Barrier(app_comm);

    //backoff variables
    size_t total_backoff_steps = 4;
    size_t backoff_times_ms[] = {500, 1000, 2500, 5000};

    //rounded up division
    size_t nb_requests = (file_size + (KILOBYTES*CHUNKSIZE) - 1) /(KILOBYTES*CHUNKSIZE);

    //bandwidth cap
    size_t request_per_microsec = 0;
    if (bandwidth_cap > 0){
        double total_time_capped_sec = file_size/(bandwidth_cap*1.0);
        request_per_microsec = (size_t)((total_time_capped_sec/nb_requests) * 1000 * 1000);
    }

    char filename[100];
    //Rtt of each request
    size_t rtt_times[nb_requests * num_phases];
    //Times at which the requests were sent
    size_t rtt_times_execution[nb_requests * num_phases];
    //Time elapsed for each io phase
    size_t periods_io[num_phases];
    //Time elapsed for each each cpu phase
    size_t periods_cpu[num_phases];
    //Times at which the phases (CPU_0, IO_0, CPU_1, IO_1 ....) started
    size_t periods_exectution[2*num_phases];
    for (int i = 0; i < num_phases; i++)
    {
        struct timespec period_start;
        struct timespec period_end;
        size_t current_file_size = file_size;


        MPI_Barrier(app_comm);

        //CPU phase
        periods_exectution[i*2] = timer_get_time_microseconds();
        timer_timestamp(&period_start, 0);
        sleep(cpu_time_sec);
        timer_timestamp(&period_end, 0);
        periods_cpu[i] = timespec_to_microseconds(period_end) - timespec_to_microseconds(period_start);

        
        //CENTRALIZED permission : ask scheduler for permission, only the master process for each application sends the request
        if (!decentralized){
            if (is_master) ask_permission_scheduler(app_id, scheduler_rank, rank, set_id);
            MPI_Barrier(app_comm);
            periods_exectution[(i*2) +1] = timer_get_time_microseconds();
            timer_timestamp(&period_start, 0);
        }

        sprintf(filename, "%s/file%d_by_%d", OUTPUT_PATH, i, rank);
        size_t offset = file_offset; 
        size_t type = RT_WRITE; 

        size_t current_backoff_step = 0;
        bool phase_accepted = false;
        struct timespec end;
        struct timespec start;
        if (is_master && decentralized) LOG("Application %d started polling server\n", app_id);
        for (size_t j = 0; j < nb_requests; j++)
        {
            int flag = REQ_FLAG_NONE;
            //set the flag if this is the last request of the last phase of the client
            if (i == num_phases - 1 && j == nb_requests - 1){
                flag = REQ_FLAG_FINAL;
                if (is_master) LOG("Application %d added final flag\n", app_id);
            }
            size_t len = current_file_size > (KILOBYTES*CHUNKSIZE) ? (KILOBYTES*CHUNKSIZE) : current_file_size;
            
            timer_timestamp(&start, 0);
            rtt_times_execution[(i*nb_requests) + j] = timer_get_time_microseconds();
            int return_code = send_request(filename, len, offset, type, set_id, flag, rank, app_id);
            timer_timestamp(&end, 0);

            //DECENTRALIZED permission : backoff until accepted by the server
            if (decentralized && !phase_accepted){
                if(is_master){
                    if(return_code == WAIT){
                        // LOG("Client %d was set to backoff\n", rank);
                        current_backoff_step += 1;
                        current_backoff_step = current_backoff_step > total_backoff_steps ? total_backoff_steps : current_backoff_step;
                        usleep(backoff_times_ms[current_backoff_step-1] * 1000);
                    }else if (return_code == SUCCESS){
                        phase_accepted = true;
                        LOG("Application %d got a SUCCESS ack from server for IO phase\n", app_id);
                        periods_exectution[(i*2) +1] = timer_get_time_microseconds();
                        timer_timestamp(&period_start, 0);
                    }

                    MPI_Bcast(&return_code, 1, MPI_INT, 0, app_comm);
                }else{
                    //reicv master message
                    MPI_Bcast(&return_code, 1, MPI_INT, 0, app_comm);
                    if (return_code == SUCCESS){
                        phase_accepted = true;
                    }
                }
            }

            rtt_times[(i*nb_requests) + j] = timespec_to_microseconds(end) - timespec_to_microseconds(start);
            offset += KILOBYTES*CHUNKSIZE;

            current_file_size -= len;

            //if capped, sleep to slow down.
            if (rtt_times[(i*nb_requests) + j] < request_per_microsec){
                // LOG("Client %d too fast for cap. Sleeping %zu microsecs\n", rank, request_per_microsec - rtt_times[(i*nb_requests) + j]);
                usleep(request_per_microsec - rtt_times[(i*nb_requests) + j]);
            }
        }

        MPI_Barrier(app_comm);
        //end of IO phase
        if (is_master) LOG("Application %d finished IO phase n° %d\n", app_id, i);
        if (decentralized && phase_accepted == false){ // if IO phase finished only by polling the server
            periods_exectution[(i*2) +1] = timer_get_time_microseconds();
            timer_timestamp(&period_start, 0);
        }
        timer_timestamp(&period_end, 0);
        periods_io[i] = timespec_to_microseconds(period_end) - timespec_to_microseconds(period_start);

        if (is_master) notify_phase_end_scheduler(app_id, scheduler_rank, app_id, set_id);
        MPI_Barrier(app_comm);

    }

    MPI_Barrier(app_comm);
    //Each client writes its rtts csv
    write_rtts(rtt_times, rtt_times_execution, nb_requests * num_phases, rank, set_id, app_id, nb_requests);
    //The master client write the phases csv
    if (is_master) write_periods(periods_io, periods_cpu, periods_exectution, num_phases*2, rank, app_id);
    MPI_Barrier(app_comm);

    // printf("Client %d is done\n", rank);
}
