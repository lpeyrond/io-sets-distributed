#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>

#include "timing.h"

struct timespec timer_program_start = {.tv_nsec = 0, .tv_sec = 0};

size_t timespec_to_microseconds(struct timespec ts) {
    return (size_t)((ts.tv_sec * 1000000LL) + (ts.tv_nsec / 1000L));
}

double timespec_to_seconds(struct timespec ts) {
    return ts.tv_sec + (ts.tv_nsec / 1e9);
}

void timer_timestamp_start(struct timespec *ts){
    if (clock_gettime(PREFERRED_CLOCK, ts) != 0){
        fprintf(stderr, "Couldn't record the timestamp\n");
        exit(EXIT_FAILURE);
    }
}

double timer_get_time_seconds(void){
    struct timespec time;
    if (clock_gettime(PREFERRED_CLOCK, &time) != 0){
        fprintf(stderr, "Couldn't record the timestamp\n");
        exit(EXIT_FAILURE);
    }
    time.tv_nsec -= timer_program_start.tv_nsec; 
    time.tv_sec -= timer_program_start.tv_sec; 

    return timespec_to_seconds(time);
}

size_t timer_get_time_microseconds(void){
    struct timespec time;
    if (clock_gettime(PREFERRED_CLOCK, &time) != 0){
        fprintf(stderr, "Couldn't record the timestamp\n");
        exit(EXIT_FAILURE);
    }
    time.tv_nsec -= timer_program_start.tv_nsec;
    time.tv_sec -= timer_program_start.tv_sec;

    return timespec_to_microseconds(time);
}

struct timespec * timer_init(int num_clients){
    assert(num_clients > 0);
    struct timespec empty_time = {.tv_nsec = 0, .tv_sec = 0};

    struct timespec * times = malloc(sizeof(*times) * num_clients);
    assert(times);
    for (int i = 0; i < num_clients; i++)
        times[i] = empty_time;

    return times;
}

void timer_timestamp(struct timespec * times, size_t index){
    if (clock_gettime(PREFERRED_CLOCK, &times[index]) != 0){
        fprintf(stderr, "Couldn't record the timestamp\n");
        exit(EXIT_FAILURE);
    }
}


void timer_free(struct timespec * times){
    if (times) free(times);
}