#pragma once
#include <stdlib.h>

typedef struct application{
    int set;                    /*Set the application belongs to*/
    int id;                     /*Id of the applcation*/
    int cpu_time;               /*Time in seconds, spent by the applcation doing computation (sleeping)*/
    int periods;                /*Number of periods*/
    int release_time;           /*Time in seconds, where the application will start sending the first requests*/
    size_t total_bytes;         /*Amount of byte being written each period by the application*/
    double bandwidth_percentage;/*Percentage of the total bandwidth used by the application*/
    char name[100];             /*Name of the application*/
} app_t;

int app_count_xml(const char * filename);

app_t *app_read_xml(const char * filename, int app_count);

void app_free(app_t *apps);

void app_print(app_t app);