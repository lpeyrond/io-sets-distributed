#define _GNU_SOURCE
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>


#include <agios.h>

#include "request_buffer.h"
#include "queue.h"
#include "request.h"
#include "timing.h"
#include "server.h"
#include "client.h"
#include "helpers.h"
#include "scheduler.h"
#include "app.h"
#include "bandwidth.h"
#include "set_usage.h"
#include "config.h"


pthread_mutex_t m=PTHREAD_MUTEX_INITIALIZER;	/* mutex lock for worker queue */
pthread_cond_t c_cons=PTHREAD_COND_INITIALIZER; /* consumer waits on this cond var */
pthread_cond_t c_prod=PTHREAD_COND_INITIALIZER; /* producer waits on this cond var */

buffer_t *request_buffer = NULL;       
queue_t *request_queue = NULL;         
int *files_descriptors = NULL;
bool flag_thread_bandwidth = true;
double ** bandwidth_table = NULL;
size_t max_bandwidth = 0;
bool decentralized = false;


int syncro_type = -1;
int slurm_job_id = -1;


void init_files(size_t client_count, int synchronize){
    files_descriptors = malloc(sizeof(*files_descriptors)*client_count);
    assert(files_descriptors);
    char filename[100];
    for (size_t i = 0; i < client_count; i++)
    {
        sprintf(filename, "%s/file0_by_%zu", OUTPUT_PATH, i + 1);
        int flags = O_WRONLY | O_CREAT | O_TRUNC;
        if (synchronize == SYNC_ODIRECT) flags = flags | O_DIRECT;
        if (synchronize == SYNC_OSYNC) flags = flags | O_SYNC;
        if (synchronize == SYNC_ODIRECT_OSYNC) flags = flags | O_DIRECT | O_SYNC;

        files_descriptors[i] = open(filename, flags, 0644);
        if (files_descriptors[i] == -1){
            fprintf(stderr, "Coudldn't open file\n");
            exit(EXIT_FAILURE);
        }
    }
}

void close_files(size_t client_count){
    for (size_t i = 0; i < client_count; i++)
        close(files_descriptors[i]);
    free(files_descriptors);
}

void write_to_disk(const request_t* req) {
    if (req == NULL) {
        fprintf(stderr, "Trying to write a NULL request\n");
        exit(EXIT_FAILURE);
    }

    ssize_t bytes_written = 0;
    if (syncro_type == SYNC_ODIRECT){
        // Determine the required alignment
        size_t alignment = sysconf(_SC_PAGESIZE); // Use the system's page size
        char *buffer = NULL;
        if (posix_memalign((void **)&buffer, alignment, CHUNKSIZE*KILOBYTES) != 0) {
            perror("Buffer allocation failed");
            exit(EXIT_FAILURE);
        }
        assert(buffer != NULL);
        memcpy(buffer, req->payload, req->len);
        if (req->len < CHUNKSIZE*KILOBYTES){
            for (size_t i = req->len+1; i < CHUNKSIZE*KILOBYTES; i++)
                buffer[i] = '\0';            
        }
        bytes_written = pwrite(files_descriptors[req->process_id - 1], buffer, CHUNKSIZE*KILOBYTES, req->offset);
        free(buffer);
    }else{
        bytes_written = pwrite(files_descriptors[req->process_id - 1], req->payload, req->len, req->offset);
    }
    

    // printf("%zu ", req->setId);
    // request_print(*req);
    if (bytes_written == -1) exit_error("Couldn't write request to file\n", __FILE__, __LINE__);
}

void *process_request(int64_t id){
    size_t index = (size_t) id;
    request_t *req = buffer_pop(request_buffer, index);
    // request_print(*req);

    //Send the request to the worker queue
    pthread_mutex_lock (&m);
    while (queue_is_full(request_queue))    /* block if queue is full */
        pthread_cond_wait (&c_prod, &m);

    /* if executing here, queue not full so add element */
    queue_enqueue(request_queue, req);

    pthread_mutex_unlock (&m);
    pthread_cond_signal (&c_cons);
    
    // printf ("producer: made request %p\n", req);
    
    return (void*)id;
}

void *consumer(void *ending_times_void)
{
    struct timespec * ending_times = (struct timespec *) ending_times_void;
    while(1){
        pthread_mutex_lock (&m);
        while (queue_is_empty(request_queue))		/* block if queue empty */
            pthread_cond_wait (&c_cons, &m);

        /* if executing here, queue not empty so remove element */
        request_t *req = queue_dequeue(request_queue);
        pthread_mutex_unlock (&m);
        pthread_cond_signal (&c_prod);
        // queue_print(request_queue);
        
        if (req->flag == REQ_FLAG_TERMINATE){
            free(req);
            break;
        }
        
        //udpate set queue_element
        if (decentralized) set_usage_inc_set(req->setId);

        write_to_disk(req);

        if (decentralized) {
            size_t set_used = set_usage_get_state();
            double allocated_percentage = set_usage_get_bandwidth_percentage(bandwidth_table, set_used, req->setId); 
            size_t set_max_bandwidth = max_bandwidth * allocated_percentage / 100;
            size_t set_bandwith = bandwidth_current(req->setId);
            int response_code = set_bandwith < set_max_bandwidth ? SUCCESS : WAIT;
            if (MPI_Send(&response_code, 1, MPI_INT, req->process_id, 0, MPI_COMM_WORLD) != MPI_SUCCESS)
                exit_error("Couldn't send request message\n", __FILE__, __LINE__);

            bandwidth_data_inc(req->setId, req->len);
            set_usage_dec_set(req->setId);
        }else{
            int response_code = SUCCESS;
            if (MPI_Send(&response_code, 1, MPI_INT, req->process_id, 0, MPI_COMM_WORLD) != MPI_SUCCESS)
                exit_error("Couldn't send request message\n", __FILE__, __LINE__);
        }

        if (!agios_release_request(req->filename, req->type, req->len, req->offset)){
            request_print(*req);
            exit_error("AGIOS couldn't release a request.\n", __FILE__, __LINE__);
        }    

        // printf ("Consume by thread %ld: request %p at time %ld\n", pthread_self(), req, tv.tv_usec);
        if (req->flag == REQ_FLAG_FINAL) {
            LOG("Final request from %zu got processed\n", req->process_id);
            if (syncro_type == SYNC_FSYNC){
                fsync(files_descriptors[req->process_id-1]);
            }
            timer_timestamp(ending_times, req->process_id-1);
        }

        request_free(req);
    }
    // printf("i'm exiting !\n");s
    pthread_exit(NULL);
}


void *metric_bandwidth(void *timestep_void){
    size_t *timestep = (size_t *)timestep_void;
    // LOG("timetep bd : %zu microsec\n", *timestep);
    while (flag_thread_bandwidth){
        bandwidth_update(*timestep);
        // for (size_t i = 0; i < 3 && bandwidth_current(i) > 0; i++)
            // LOG("Current bandwidth of set %zu is %zu\n", i, bandwidth_current(i));
        usleep(*timestep);
    }

    LOG("thread bandwidth exiting\n");
    pthread_exit(NULL);
}


void server(size_t num_consumers, size_t num_sets, size_t buffer_size, size_t queue_size, size_t num_clients, struct timespec * start_times, struct timespec * end_times, int scheduler_rank){
    request_buffer = buffer_create(buffer_size);
    request_queue = queue_create(queue_size);

    // printf("Creating %zu consumer threads\n", num_consumers);
    pthread_t consumers_id[num_consumers];
    for (size_t i = 0; i < num_consumers; i++){
        if (pthread_create(&consumers_id[i], NULL, consumer, (void *)end_times) != 0)
        {
	    	fprintf (stderr, "Unable to create consumer thread\n");
	    	exit (1);
	    }
    }

    pthread_t updater_id = 0;
    if (decentralized){
        size_t timestep = BANDWIDTH_COMPUTATION_TIMESTEP_MICROSECONDS;
        if (pthread_create(&updater_id, NULL, metric_bandwidth, (void *)&timestep) != 0)
        {
            fprintf (stderr, "Unable to create bandwidth updater thread\n");
            exit (1);
        }
    }

    char *envvar_conf = "AGIOS_CONF";
    int buf_size = 100;
    char file_config[buf_size];

	if(!getenv(envvar_conf) || snprintf(file_config, buf_size, "%s", getenv(envvar_conf)) >= buf_size){
        fprintf(stderr, "The environment variable %s was not found or the BUFSIZE of %d was to small .\n", envvar_conf, buf_size);
        exit(EXIT_FAILURE);
    }


    if (!agios_init(process_request, NULL, file_config, num_sets)){
        fprintf(stderr, "Agios initialisation failed\n");
        exit(1);
    }

    //server ready
    //receiving until clients are done
    size_t remaining_clients = num_clients;
    while(remaining_clients > 0)
    {   
        request_t *recv_request = request_create("tmp", 0, 0, 0, 0, 0, REQ_FLAG_NONE, 0); //just getting a request_t memory allocation, content will get overwrited by the MPI receive
        if (MPI_Recv(recv_request, 1, mpi_request_t, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE) != MPI_SUCCESS){
            fprintf(stderr, "Couldn't receive a message\n");
            exit(EXIT_FAILURE);
        }

        //inits start time of the request process if not already done
        if (start_times[recv_request->process_id - 1].tv_nsec == 0 && start_times[recv_request->process_id - 1].tv_sec == 0){
            timer_timestamp(start_times, recv_request->process_id-1);
        }

        //storing request fields that will be used later on as the request can already be processed and freed when needed 
        int req_flag = recv_request->flag;
        // size_t req_id = recv_request->process_id;
        // printf("storing request\n");
        //storing the request
        size_t index = buffer_get_index(request_buffer);
        if (index == UINT_MAX){
            fprintf(stderr, "Request buffer is full\n");
            //drop request ?
            exit(1);
        }
        buffer_add(request_buffer, recv_request, index);
        // printf("buffer elements : %zu\n",request_buffer->size - request_buffer->stack_top);
        //sending it to agios
        if (!agios_add_request(recv_request->filename, recv_request->type, recv_request->offset, recv_request->len, index, recv_request->setId)){
            fprintf(stderr, "Adding request failed for request\n");
            exit(1);
        }

        //waiting until buffer isn't full to send response to not overflow the buffer
        if(buffer_is_full(request_buffer)){
            while(request_buffer->size - request_buffer->stack_top > request_buffer->size/2 ){
                usleep(1);
                // printf("buf full sleep\n");
            }
        }
        
        //client will not send anymore requests
        if (req_flag == REQ_FLAG_FINAL){
            remaining_clients--;
            // printf("remaning clients : %zu\n", remaining_clients);
        }
    }

    //waiting for all requests to be handled
    LOG("Server waiting until buffer is empty\n");
    while(!buffer_is_empty(request_buffer)){
        usleep(250);
    }
    buffer_free(request_buffer);

    // printf("Waiting for queue emptying\n");
    LOG("Server waiting until queue is empty\n");
    while(!queue_is_empty(request_queue)){
        usleep(250);
    }

    // Send specific request that terminate the thread reading it
    LOG("Server terminating threads\n");
    for (size_t i = 0; i < num_consumers; i++){
        pthread_mutex_lock (&m);
        while (queue_is_full(request_queue))    /* block if queue is full */
            pthread_cond_wait (&c_prod, &m);

        request_t *req = request_create("thread_exit", 0,0,0,0,0, REQ_FLAG_TERMINATE, 0);
        queue_enqueue(request_queue, req);

        pthread_mutex_unlock (&m);
        pthread_cond_signal (&c_cons);
    }

    // Wait For all the consumer threads to finish
    for (size_t i = 0; i < num_consumers; i++){
        if (pthread_join(consumers_id[i], NULL) != 0){
            fprintf(stderr, "Error joining consumer thread\n");
            exit(1);
        }
    }

    if (decentralized){
        // Wait For all the bandwidth updater thread to finish
        flag_thread_bandwidth = false;
        if (pthread_join(updater_id, NULL) != 0){
            fprintf(stderr, "Error joining consumer thread\n");
            exit(1);
        }
    }


    //sending message to end scheduler
    scheduling_info_t message = {.app_id = -1, .client_rank = -1, .set_id = -1, .flag = EXECUTION_FINISHED};
    if (MPI_Send(&message, 1, mpi_scheduling_info_t, scheduler_rank, 0, MPI_COMM_WORLD) != MPI_SUCCESS)


    LOG("Server freeing queue and agios then exiting\n");
    queue_free(request_queue);
    agios_exit();

}


void write_csv_parameters(size_t thread_count, int app_count, int client_count, char *xml_path, size_t benchmarked_bandwidth, unsigned int seed, size_t set_count, int *set_priorities, char * execution_label){

    char filename[150];
    sprintf(filename, "%s/logs/%d_parameters.csv", getenv("HOME"), slurm_job_id);

    FILE *csv_file = fopen(filename, "w");
    
    if (csv_file == NULL) {
        fprintf(stderr, "Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fprintf(csv_file, "JobId, Param_Threads_Count, Param_App_Count, Param_Clients_Count, Param_Decentralized, Param_Sync_Mode, Param_XLM_Path, Param_Benchmarked_Bandwidth,  Param_Bandwidth_Multiplier, Param_Bandwidth_Timestep, Param_Seed, Param_Chunksize, Param_Set_Count, Param_Set_Priorities, Param_Label\n");  // Header row
    fprintf(csv_file, "%d, %zu, %d, %d, %d, %d, %s, %zu, %.3lf, %d, %d, %d, %zu", slurm_job_id, thread_count, app_count, client_count, decentralized, syncro_type, xml_path, benchmarked_bandwidth, BANDWIDTH_MULTIPLIER, BANDWIDTH_COMPUTATION_TIMESTEP_MICROSECONDS, seed, CHUNKSIZE*KILOBYTES, set_count);
    //printing the set priorites
    for (size_t i = 0; i < set_count; i++){
        if(i == 0) fprintf(csv_file, ", [%d ", set_priorities[i]);
        else if(i == (set_count-1)) fprintf(csv_file, "%d]", set_priorities[i]);
        else fprintf(csv_file, "%d ", set_priorities[i]);
    }
    fprintf(csv_file, ", %s\n", execution_label);

    fclose(csv_file);
}

void write_csv_applications(int app_count, app_t * apps){

    char filename[150];
    sprintf(filename, "%s/logs/%d_applications.csv", getenv("HOME"), slurm_job_id);

    FILE *csv_file = fopen(filename, "w");
    
    if (csv_file == NULL) {
        fprintf(stderr, "Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fprintf(csv_file, "JobId, App_Id, App_Data, App_Cpu_Time, App_Bandwidth_Limit, App_Set, App_Periodicity, App_Release_Time, App_Name\n");  // Header row
    for (int i = 0; i < app_count; i++){
        app_t *app = apps + i;
        fprintf(csv_file, "%d, %d, %zu, %d, %.2lf, %d, %d, %d, %s\n", slurm_job_id, app->id, app->total_bytes, app->cpu_time, app->bandwidth_percentage, app->set, app->periods, app->release_time, app->name);
    }

    fclose(csv_file);
}



char * sync_type_to_str(enum sync_types t){
    if (t == 0){
        return "None";
    }else if (t == 1){
        return "O_DIRECT";
    }else if (t == 2){
        return "O_SYNC";
    }else if (t == 3){
        return "O_DIRECT+O_SYNC";
    }else if (t == 4){
        return "FSYNC";
    }
    exit_error("Tried to convert out of range sync type to string\n", __FILE__, __LINE__);
    return NULL;
}


void usage(const char *name){
    fprintf(stderr, "Usage: %s -t <threads> -s <sync_type> -x <XML_path> -d <decentralized [0,1]> -l \"label\" [-S <seed>]\n  Sync types : \n\t0 : %s\n\t1 : %s\n\t2 : %s\n\t3 : %s\n\t4 : %s\n", name, 
        sync_type_to_str(0), sync_type_to_str(1), sync_type_to_str(2), sync_type_to_str(3), sync_type_to_str(4));
}

int main(int argc, char ** argv)
{
    struct timespec * start_times = NULL;
    struct timespec * end_times = NULL;
    timer_timestamp_start(&timer_program_start);

    //Initialize the MPI environment. MPI in Plafrim doesn't need Init_Thread but local computer does
    if (is_running_in_cluster()){
        MPI_Init(NULL, NULL);
    }else{
        int provided;
        MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
        if (provided < MPI_THREAD_MULTIPLE)
        {
            fprintf(stderr, "ERROR: The MPI library does not have full thread support\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }
    
    //Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    //Get and commit request mpi datatype for client and scheduler messages
    mpi_request_t = request_create_mpi_type();
    MPI_Type_commit(&mpi_request_t);
    mpi_scheduling_info_t = scheduling_info_create_mpi_type();
    MPI_Type_commit(&mpi_scheduling_info_t);

    //Getting arguments of the program
    unsigned int seed = 123;
    size_t num_consumers = 0;
    int decentralized_arg = -1;
        char xml_path[200];
    xml_path[0] = '\0';
    char execution_label[200];
    execution_label[0] = 'N';
    execution_label[1] = 'o';
    execution_label[2] = 'n';
    execution_label[3] = 'e';
    execution_label[4] = '\0';

    int opt = -1;
    while ((opt = getopt(argc, argv, "t:s:x:d:l:S:")) != -1) {
        switch (opt) {
            case 't':
                num_consumers = atoi(optarg);
                break;
            case 'S':
                seed = atoi(optarg);
                break;
            case 's':
                syncro_type = atoi(optarg);
                break;
            case 'x':
                strcpy(xml_path, optarg);
                break;
            case 'd':
                decentralized_arg = atoi(optarg);
                break;

            case 'l':
                strcpy(execution_label, optarg);
                break;

            default:
                usage(argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (num_consumers == 0 || syncro_type == -1 || xml_path[0] == '\0' || !(decentralized_arg == 0 || decentralized_arg == 1)){
        if (world_rank == 0) usage(argv[0]);
        exit(EXIT_FAILURE);
    }
    
    decentralized = decentralized_arg == 1;
    srand(seed);

    //XML scenarios reading
    if (world_rank == 0) LOG("Reading XML configuration file at %s\n", xml_path);
    int num_apps = app_count_xml(xml_path);
    if (num_apps == 0) exit_error("No applications could be read from the XML file\n", __FILE__, __LINE__);
    app_t * applications = app_read_xml(xml_path, num_apps);
    if (world_rank == 0) LOG("Finished reading XML configuration file\n");
    
    //Computing the total amount of bytes that will be written by the scenario 
    size_t total_data_size = 0;
    for (int i = 0; i < num_apps; i++)
        total_data_size += applications[i].total_bytes * applications[i].periods;


    int num_clients = world_size -2; //minus server and scheduler process
    if (num_clients % num_apps != 0) exit_error("Clients cannot be evenly distributed to applications\n", __FILE__, __LINE__);
    int scheduler_rank = world_size - 1;
    int server_rank = 0;

    size_t queue_size = REQUEST_QUEUE_SIZE;
    size_t buffer_size = REQUEST_BUFFER_SIZE;

    size_t num_sets = NUMBER_OF_SETS;
    if(num_sets > MAXIMUM_NUMBER_OF_SETS) exit_error("Too many set used\n", __FILE__, __LINE__);
    int set_priorities[] = SETS_PRIORITIES;

    //If scheduled by SLURM in a cluster, set the global execution id to the job_id
    set_execution_id(world_rank, server_rank);
    
    
    size_t benchmarked_bandwidth = 0;
    if(world_rank == server_rank){
        LOG("Starting for %zu Threads with synchronization : %s. %d Applications with %d clients each. Seed used : %d, label is \"%s\"\n", num_consumers, sync_type_to_str(syncro_type), num_apps, num_clients/num_apps, seed, execution_label);
        LOG("\tMode used is %s for a total of %.3lf Gigabytes\n", decentralized ? "DECENTRALIZED" : "CENTRALIZED", total_data_size/1000.0/1000.0/1000.0);
        LOG("\tDetail of the applications loaded : \n");
        for (int i = 0; i < num_apps; i++){
            printf("\t\t");
            app_print(applications[i]);
        }        
        start_times = timer_init(num_clients);
        end_times = timer_init(num_clients);
        init_files(num_clients, syncro_type);
        if (decentralized){
            bandwidth_init(num_sets);
            set_usage_init(num_sets);
            bandwidth_table = set_usage_compute_bandwidth_table(num_sets, set_priorities);
        }
        LOG("Starting bandwidth benchmark\n");
        benchmarked_bandwidth = benchmark_max_bandwidth(syncro_type);

        //This value is used as the maximum instead of the benchmarked one
        max_bandwidth = benchmarked_bandwidth * BANDWIDTH_MULTIPLIER; 
        LOG("Ended bandwidth benchmark : Max Bandwidth is %.3lfMB/s (%.2lf percent from the benchmarked results)\n", max_bandwidth/1000.0/1000.0, BANDWIDTH_MULTIPLIER*100);
    }

    //Shares the max bandwidth between processes  
    MPI_Bcast(&max_bandwidth, 1, my_MPI_SIZE_T, server_rank, MPI_COMM_WORLD);
    
    MPI_Barrier(MPI_COMM_WORLD);
    size_t clients_per_app = num_clients/num_apps;

    //MPI_Communicator that allows each process/client of the same application to communicate with each others. 
    MPI_Comm application_comm;

    //Launch the server, scheduler and clients
    if (world_rank == server_rank){
        LOG("Server is assigned to rank %d\n", world_rank);
        MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, world_rank, &application_comm);
        server(num_consumers, num_sets, buffer_size, queue_size, num_clients, start_times, end_times, scheduler_rank);
    }else if (world_rank == scheduler_rank){
        MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, world_rank, &application_comm);
        if (!decentralized) {
            LOG("Scheduler is assigned to rank %d\n", world_rank);
            scheduler(num_sets);
        }
    }else{
        app_t *app = &applications[(world_rank-1)%num_apps];
        size_t file_size_per_client = app->total_bytes / clients_per_app;
        size_t app_bandwidth_cap = max_bandwidth * (app->bandwidth_percentage/100);
        int master_rank = ((world_rank-1)%num_apps) +1;
        MPI_Comm_split(MPI_COMM_WORLD, app->id, world_rank, &application_comm);

        client(file_size_per_client, 0, app->periods, app->set, world_rank, app->id, app->cpu_time, scheduler_rank, master_rank, application_comm, app->release_time, app_bandwidth_cap/clients_per_app, decentralized);

        MPI_Comm_free(&application_comm);
    }


    MPI_Barrier(MPI_COMM_WORLD);

    //Freeing allocations and writing csv
    if (world_rank == server_rank){
        LOG("Main process cleaning environment\n");
        close_files(num_clients);
        write_csv_parameters(num_consumers, num_apps, num_clients, xml_path, max_bandwidth, seed, num_sets, set_priorities, execution_label);
        write_csv_applications(num_apps, applications);
        timer_free(start_times);
        timer_free(end_times);
        if (decentralized){
            bandwidth_free();
            set_usage_free();
            set_usage_free_bandwidth_table(bandwidth_table, num_sets);
        }
    }
    
    app_free(applications);
    
    MPI_Barrier(MPI_COMM_WORLD);
    //Finalize the MPI environment.
    MPI_Type_free(&mpi_request_t);
    MPI_Type_free(&mpi_scheduling_info_t);
    MPI_Finalize();


    return 0;
}
