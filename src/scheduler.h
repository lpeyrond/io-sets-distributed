#pragma once

#include <stdlib.h>
#include <mpi.h>

enum scheduling_flag{
    ASK_PERMISSION = 1,
    PHASE_FINISHED,
    EXECUTION_FINISHED
};


typedef struct scheduling_info {
    enum scheduling_flag flag;
    int client_rank;
    int app_id;
    int set_id;
} scheduling_info_t;

extern MPI_Datatype mpi_scheduling_info_t;


MPI_Datatype scheduling_info_create_mpi_type();
void scheduler(size_t set_count);