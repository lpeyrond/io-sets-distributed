#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>

#include "helpers.h"
#include "timing.h"
#include "server.h"


void exit_error(char * error_msg, const char * filename, int line){
    fprintf(stderr, "[%.6lf] ERROR %d %s:%d : %s", timer_get_time_seconds(), errno, filename, line, error_msg);
    exit(EXIT_FAILURE);
}

int count_files_in_folder(const char* folder_path) {
    int count = 0;
    DIR* dir = opendir(folder_path);
    if (dir) {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { // Check if it's a regular file
                count++;
            }
        }
        closedir(dir);
    } else {
        exit_error("Failed to open folder\n", __FILE__, __LINE__);
    }
    return count;
}

bool is_running_in_cluster(){
    char *SLURM_ENV_VAR = "SLURM_JOB_ID";
    return getenv(SLURM_ENV_VAR);
}

void set_execution_id(int rank, int server_rank){
    char *SLURM_ENV_VAR = "SLURM_JOB_ID";
    int buf_size = 100;
    char job_id[buf_size];
    if(!getenv(SLURM_ENV_VAR)){
        char folder_path[150];
        sprintf(folder_path, "%s/logs/", getenv("HOME"));
        slurm_job_id = count_files_in_folder(folder_path);
        if (rank == server_rank) LOG("Couldn't find slurm job id env var for naming. Using next value in folder instead : %d\n", slurm_job_id);
    }else{
        if(snprintf(job_id, buf_size, "%s", getenv(SLURM_ENV_VAR)) >= buf_size){
            fprintf(stderr, "BUFSIZE of %d was too small. Aborting\n", buf_size);
            exit(1);
        }
        slurm_job_id = atoi(job_id);
    }
}
