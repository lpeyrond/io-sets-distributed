#include "queue.h"

#include <stdio.h>
#include <pthread.h>


queue_t *queue_create(size_t length){
    queue_t *q = malloc(sizeof(*q));
    if (!q){
        fprintf(stderr, "Couldn't allocate memory for queue creation\n");
        exit(EXIT_FAILURE);
    }

    q->readIndex = 0;
    q->writeIndex = 0;

    q->elements = malloc(sizeof(*q->elements)*(length+1));
    if (!q->elements){
        fprintf(stderr, "Couldn't allocate memory for queue creation\n");
        exit(EXIT_FAILURE);
    }
    
    q->max_length = length+1;
    q->element_count = 0;
    pthread_mutex_init(&q->mutex, NULL);

    return q;
}

void queue_enqueue(queue_t* q, void *request){
    pthread_mutex_lock(&q->mutex);
    if (queue_is_full(q)){
        fprintf(stderr, "Adding into a full queue\n");
    }

    q->elements[q->writeIndex] = request;
    q->element_count++;
    q->writeIndex = (q->writeIndex + 1) % q->max_length;
    pthread_mutex_unlock(&q->mutex);
}

void * queue_dequeue(queue_t* q){
    pthread_mutex_lock(&q->mutex);
    if (queue_is_empty(q))
    {
        fprintf(stderr, "Dequeue on an empty queue\n");
        exit(EXIT_FAILURE);
    }

    void *r = q->elements[q->readIndex];
    q->readIndex = (q->readIndex + 1) % q->max_length;
    q->element_count--;
    pthread_mutex_unlock(&q->mutex);
    return r;
}

bool queue_is_empty(queue_t* q){
    return q->element_count == 0;
}

bool queue_is_full(queue_t* q){
    return q->element_count == q->max_length - 1;
}

void queue_free(queue_t* q){
    if(q){
        if(q->elements) free(q->elements);
        free(q);
    }
}

void queue_print(queue_t* q){
    pthread_mutex_lock(&q->mutex);
    // printf("read at %zu, write at %zu, elm count %zu\n",q->readIndex, q->writeIndex, q->element_count);
    size_t ids[3] = {0,0,0};
    for (size_t i = 0; !queue_is_empty(q) && i < q->element_count; i++)
    {
        // printf(" %zu", q->elements[(q->readIndex + i)%q->max_length]->setId);
        request_t *req = q->elements[(q->readIndex + i)%q->max_length];
        ids[req->setId] += 1;
    }
    printf("0 : %zu, 1 : %zu 2 : %zu\n", ids[0], ids[1], ids[2]);
    pthread_mutex_unlock(&q->mutex);
    
}
