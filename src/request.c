#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <string.h>

#include "request.h"


MPI_Datatype mpi_request_t;

void request_print(request_t r){
    printf("file : %s, size : %zu, offset : %zu, type : %zu, setId : %zu, process : %zu, flag : %d\n", r.filename, r.len, r.offset, r.type, r.setId, r.process_id, r.flag);
}

request_t * request_create(const char *filename, size_t len, size_t offset, size_t type, size_t setId, size_t process_id, int flag, size_t app_id){
    request_t *r = malloc(sizeof(*r));
    assert(r);
    strncpy(r->filename, filename, sizeof(r->filename) - 1);
    r->filename[sizeof(r->filename) - 1] = '\0'; 
    r->offset = offset;
    r->len = len;
    r->type = type;
    r->app_id = app_id;
    r->setId = setId;
    r->process_id = process_id;
    r->flag = flag;
    memset(r->payload, 6, len);
    return r;
}

void request_free(request_t *r){
    if (r) free(r);
}

MPI_Datatype request_create_mpi_type(){
    MPI_Datatype mpi_request_t;

    int blocklengths[] = {1, 1, 1, 1, 1, 1, 1, FILENAME_MAXSIZE, CHUNKSIZE * KILOBYTES};  // Number of elements in each block
    MPI_Datatype types[] = {MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_INT, MPI_CHAR, MPI_CHAR};
    MPI_Aint offsets[] = {
        offsetof(request_t, len),
        offsetof(request_t, offset),
        offsetof(request_t, type),
        offsetof(request_t, setId),
        offsetof(request_t, process_id),
        offsetof(request_t, app_id),
        offsetof(request_t, flag),
        offsetof(request_t, filename),
        offsetof(request_t, payload)
    };
    MPI_Type_create_struct(9, blocklengths, offsets, types, &mpi_request_t);

    return mpi_request_t;
}

size_t gigabytes(size_t x){
    return 1000 * 1000 * 1000 * x;
}

size_t megabytes(size_t x){
    return 1000 * 1000 * x;
}