#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <pthread.h>

#include "request_buffer.h"


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


buffer_t * buffer_create(size_t size){
    if (size >= UINT_MAX){
        fprintf(stderr, "Buffer size is too big. Should be < UINT_MAX\n");
        exit(EXIT_FAILURE);
    }

    buffer_t * buf = malloc(sizeof(*buf));
    if (!buf){
        fprintf(stderr, "Couldn't allocate memory for buffer creation\n");
        exit(EXIT_FAILURE);
    }

    buf->requests = malloc(sizeof(*buf->requests) * size);
    if (!buf->requests){
        fprintf(stderr, "Couldn't allocate memory for buffer creation\n");
        exit(EXIT_FAILURE);
    }

    buf->index_stack = malloc(sizeof(*buf->index_stack) * size);
    if (!buf->index_stack){
        fprintf(stderr, "Couldn't allocate memory for buffers index_stack creation\n");
        exit(EXIT_FAILURE);
    }
    buf->size = size;

    //populating the stack with the free indexes
    for (size_t i = 0; i < size; i++)
    {
        buf->index_stack[i] = size - i - 1;
    }
    buf->stack_top = size;
    

    return buf;
}


void buffer_add(buffer_t *buf, request_t *request, size_t index){
    assert(index < buf->size);

    buf->requests[index] = request;
}

size_t buffer_get_index(buffer_t *buf){
    //buffer is full
    if (buf->stack_top == 0){
        return UINT_MAX;
    }

    pthread_mutex_lock(&mutex);
    buf->stack_top--;
    size_t index = buf->index_stack[buf->stack_top];
    pthread_mutex_unlock(&mutex);
    return index;
}

request_t *buffer_pop(buffer_t *buf, size_t index){
    assert(index < buf->size);
    assert(buf->stack_top < buf->size);

    pthread_mutex_lock(&mutex);
    buf->index_stack[buf->stack_top] = index;
    buf->stack_top++;
    request_t *r = buf->requests[index];
    pthread_mutex_unlock(&mutex);
    
    
    assert(buf->stack_top <= buf->size);
    return r;
}


bool buffer_is_empty(buffer_t *buf){
    return buf->stack_top == (buf->size);
}

bool buffer_is_full(buffer_t *buf){
    return buf->stack_top == 0;
}

void buffer_free(buffer_t *buf){
    if (buf){
        if (buf->index_stack) free(buf->index_stack);
        if (buf->requests) free(buf->requests);
        free(buf);
    }
}
