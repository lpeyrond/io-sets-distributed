#pragma once

#include <stdlib.h>
#include <mpi.h>

#include "config.h"

//for opaque implementation
// typedef struct request request_t;

#define FILENAME_MAXSIZE 100

#define REQ_FLAG_NONE 0
#define REQ_FLAG_FINAL 1
#define REQ_FLAG_TERMINATE 2

typedef struct request {
    size_t len;
    size_t offset;
    size_t type;
    size_t setId;
    size_t process_id;
    size_t app_id;
    int flag;
    char filename[FILENAME_MAXSIZE];
    char payload[CHUNKSIZE*KILOBYTES];
} request_t;

extern MPI_Datatype mpi_request_t;


void request_print(request_t r);
request_t * request_create(const char *filename, size_t len, size_t offset, size_t type, size_t setId, size_t process_id, int flag, size_t app_id);
void request_free(request_t *r);
size_t gigabytes(size_t x);
size_t megabytes(size_t x);

/// @brief The returned mpi type should be commited using MPI_Type_commit and then freed with MPI_Type_free
/// @return 
MPI_Datatype request_create_mpi_type();