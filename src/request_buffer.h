#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include "request.h"

typedef struct buffer {
    size_t size;            //Maximum size of the buffer
    size_t *index_stack;    //Stack that stores the available indexes for insertion
    size_t stack_top;       //Number of element in the stack
    request_t **requests;   //The actual buffer, array of pointers on requests
} buffer_t;



buffer_t * buffer_create(size_t size);

/// @brief Get a free index in the buffer to allow insertion. The returned index MUST be used. If they are no index available UINT_MAX is returned.
/// @param buf The buffer
/// @return The available index
size_t buffer_get_index(buffer_t *buf);

/// @brief Adds the given request to the buffer at the given index. The index should ALWAYS come from the get_index function.
/// @param buf The buffer
/// @param request The pointer to a request
/// @param index The index to insert. MUST come from the get_index function.
void buffer_add(buffer_t *buf, request_t *request, size_t index);

/// @brief Get the request stored at the given index and removes it from the buffer, making the index avaible. 
/// @param buf The buffer
/// @param index The index
/// @return The pointer on the request at the index
request_t *buffer_pop(buffer_t *buf, size_t index);
bool buffer_is_empty(buffer_t *buf);
bool buffer_is_full(buffer_t *buf);

/// @brief Frees the memory allocated for the buffer. It doesn't free the items stored inside.
/// @param buf The buffer
void buffer_free(buffer_t *buf);




