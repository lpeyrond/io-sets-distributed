#pragma once
#include <stdlib.h>


void bandwidth_init(size_t set_count);

void bandwidth_data_inc(int set_id, size_t data_size);

void bandwidth_update(size_t timestep);

size_t bandwidth_current(int set_id);

void bandwidth_free();

size_t benchmark_max_bandwidth(int synchronize);