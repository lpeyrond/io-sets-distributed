#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "../src/request_buffer.h"



int main()
{
    size_t size = 64; 
    buffer_t *buf = buffer_create(size);
    assert(buffer_is_empty(buf));
    assert(!buffer_is_full(buf));

    for (size_t i = 0; i < size; i++)
    {
        // printf("adding %zu, stack top is %zu\n", i, buf->stack_top);
        size_t index = buffer_get_index(buf);
        buffer_add(buf, NULL, index);
        if (i != (size - 1) ){
            assert(!buffer_is_full(buf));
            assert(!buffer_is_empty(buf));
        }
    }
    assert(buffer_is_full(buf));
    //check that we get UINTMAX when asking for a index on a full buffer
    assert(buffer_get_index(buf) == UINT_MAX);

    request_t * req = buffer_pop(buf, size/2);
    req = req;
    assert(!buffer_is_full(buf));

    size_t index = buffer_get_index(buf);
    assert(index == size/2);
    buffer_add(buf, NULL, index);
    req = buffer_pop(buf, index);
    

    for (size_t i = 0; i < size - 1; i++)
    {
        assert(!buffer_is_full(buf));
        assert(!buffer_is_empty(buf));
        req = buffer_pop(buf, i);
    }
    assert(buffer_is_empty(buf));
    
    buffer_free(buf);

    printf("test buffer ok\n");
    return 0;
}
