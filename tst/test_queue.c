#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "../src/queue.h"



int main()
{
    size_t size = 8; 

    queue_t *q = queue_create(size);
    assert(queue_is_empty(q));

    // queue_print(q);

    queue_enqueue(q, NULL);
    assert(!queue_is_empty(q));
    // queue_print(q);

    request_t *req = queue_dequeue(q);
    req = req;
    assert(queue_is_empty(q));

    // queue_print(q);


    for (size_t i = 0; i < size; i++)
    {
        printf("inserting %zu\n", i);
        queue_enqueue(q, (request_t *)i);
        assert(!queue_is_empty(q));
        if (i != size - 1) assert(!queue_is_full(q));
        // queue_print(q);
    }
    
    assert(queue_is_full(q));

    req = queue_dequeue(q);
    assert(req == 0);
    assert(!queue_is_full(q));
    assert(!queue_is_empty(q));
    queue_enqueue(q, (request_t *)8);
    // queue_print(q);

    for (size_t i = 0; i < size; i++)
    {
        printf("removing from queue\n");
        req = queue_dequeue(q);
        assert(req == (request_t *)(i + 1));
        if (i != size - 1) assert(!queue_is_empty(q));
        assert(!queue_is_full(q));
        // queue_print(q);
    }
    

    assert(queue_is_empty(q));

    queue_free(q);

    printf("test queue ok\n");
    return 0;
}
